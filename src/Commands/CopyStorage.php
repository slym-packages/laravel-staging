<?php

namespace Slym\LaravelStaging\Commands;

use Illuminate\Console\Command;
use Slym\LaravelStaging\Support\ShellCommand;

class CopyStorage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'slym:staging:storage';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import prod storage to staging';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        throw_if(app()->environment() !== 'production', new \RuntimeException('Enable only on production !'));

        $stagingPath = config('staging.root');

        throw_if(empty($stagingPath), new \RuntimeException('Staging not configured !'));

        ShellCommand::execute('rsync -rv --delete '.storage_path('app').'/ '.$stagingPath.'storage/app/');

        $this->info('Staging storage copied !');

        return 0;
    }
}
