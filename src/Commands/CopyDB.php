<?php

namespace Slym\LaravelStaging\Commands;

use Dotenv\Dotenv;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Config;

class CopyDB extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'slym:staging:db';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import prod db to staging';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        throw_if(app()->environment() !== 'production', new \RuntimeException('Enable only on production !'));

        $stagingPath = config('staging.root');

        throw_if(empty($stagingPath), new \RuntimeException('Staging not configured !'));

        $config = Dotenv::createArrayBacked($stagingPath)->load();

        Config::set('database.connections.mysql-staging.database', $config['DB_DATABASE']);
        Config::set('database.connections.mysql-staging.username', $config['DB_USERNAME']);
        Config::set('database.connections.mysql-staging.password', $config['DB_PASSWORD']);

        $this->call('snapshot:create');

        $this->call('snapshot:load', [
            '--force' => true,
            '--latest' => true,
            '--connection' => 'mysql-staging',
        ]);

        $this->call('snapshot:cleanup', ['--keep' => '0']);

        return 0;
    }
}
