<?php

namespace Slym\LaravelStaging\Commands;

use Illuminate\Console\Command;

class Reset extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'slym:staging:reset';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset staging with prod';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        throw_if(app()->environment() !== 'production', new \RuntimeException('Enable only on production !'));

        $this->call('slym:staging:db');
        $this->call('slym:staging:storage');

        return 0;
    }
}
