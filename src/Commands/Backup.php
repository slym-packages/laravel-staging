<?php

namespace Slym\LaravelStaging\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Config;

class Backup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'slym:staging:backup {only}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Backup';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $only = $this->argument('only');

        throw_if(
            in_array($only, ['db', 'storage', 'app']) === false,
            new \Exception('Unknown value on argument only')
        );

        $directory = preg_replace(
            '#^(https?://)?(www\d?\.)?#i',
            '',
            Config::get('app.url')
        );

        $args = [
            '--only-to-disk' => 'sftp-backup',
        ];

        if ($only === 'db') {
            $name = 'db';
            $args['--only-db'] = true;
        } elseif ($only === 'storage') {
            $name = 'storage';

            Config::set('backup.backup.source.files.include', [
                storage_path('app'),
            ]);

            Config::set('backup.backup.source.files.exclude', [
                storage_path('app/backup-temp'),
                storage_path('app/laravel-backup'),
                storage_path('app/tmp'),
            ]);

            $args['--only-files'] = true;
        } else {
            $name = 'app';

            Config::set('backup.backup.source.files.include', [
                base_path(),
            ]);

            Config::set('backup.backup.source.files.exclude', [
                base_path('vendor'),
                base_path('node_modules'),
                base_path('storage'),
            ]);

            $args['--only-files'] = true;
        }

        $sftp = Config::get('staging.backup.sftp');
        $sftp['root'] = "{$directory}/";

        Config::set('backup.backup.name', $name);
        Config::set('backup.backup.destination.disks', ['sftp-backup']);
        Config::set('filesystems.disks.sftp-backup', $sftp);

        $this->call('backup:run', $args);

        return 0;
    }
}
