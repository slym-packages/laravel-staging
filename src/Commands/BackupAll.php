<?php

namespace Slym\LaravelStaging\Commands;

use Illuminate\Console\Command;

class BackupAll extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'slym:staging:backup-all';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Backup';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('Backup starting at '.now()->format('Y-m-d H:i:s'));

        $this->call('slym:staging:backup', [
            'only' => 'db',
        ]);

        $this->info('Backup db ending at '.now()->format('Y-m-d H:i:s'));

        $this->call('slym:staging:backup', [
            'only' => 'app',
        ]);

        $this->info('Backup app ending at '.now()->format('Y-m-d H:i:s'));

        $this->call('slym:staging:backup', [
            'only' => 'storage',
        ]);

        $this->info('Backup ending at '.now()->format('Y-m-d H:i:s'));

        return 0;
    }
}
