<?php

namespace Slym\LaravelStaging\Http\Controllers;

use Dotenv\Dotenv;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Http;

class ResetController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __invoke(Request $request)
    {
        if (app()->environment() !== 'production') {
            $productionPath = config('staging.production');

            throw_if(empty($productionPath), new \RuntimeException('Production not configured !'));

            $config = Dotenv::createArrayBacked($productionPath)->load();

            $response = Http::get($config['APP_URL'].'/staging/reset');

            return response()->json(
                $response->json()
            );
        }

        if ($request->has('db')) {
            Artisan::call('slym:staging:db');
        }

        if ($request->has('storage')) {
            Artisan::call('slym:staging:storage');
        }

        return response()->json(['status' => 'ok']);
    }
}
