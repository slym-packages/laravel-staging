<?php

namespace Slym\LaravelStaging;

use Illuminate\Support\Facades\Mail;
use Slym\LaravelStaging\Commands\Backup;
use Slym\LaravelStaging\Commands\BackupAll;
use Slym\LaravelStaging\Commands\CopyDB;
use Slym\LaravelStaging\Commands\CopyStorage;
use Slym\LaravelStaging\Commands\Reset;
use Spatie\LaravelPackageTools\Package;
use Spatie\LaravelPackageTools\PackageServiceProvider;

class LaravelStagingServiceProvider extends PackageServiceProvider
{
    public function configurePackage(Package $package): void
    {
        /*
         * This class is a Package Service Provider
         *
         * More info: https://github.com/spatie/laravel-package-tools
         */
        $package
            ->name('laravel-staging')
            ->hasRoute('web')
            ->hasConfigFile()
            ->hasCommands([
                CopyDB::class,
                CopyStorage::class,
                Reset::class,
                Backup::class,
                BackupAll::class,
            ]);
    }

    public function bootingPackage(): void
    {
        if ($this->app->environment('production') === false
            && config('staging.mail_always_to_address')) {
            Mail::alwaysTo(config('staging.mail_always_to_address'));
        }
    }
}
