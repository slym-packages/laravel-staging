# Manage staging website

## Installation

You can install the package via composer:

```bash
composer require slym/laravel-staging
```

You can publish the config file with:
```bash
php artisan vendor:publish --tag="laravel-staging-config"
```

This is the contents of the published config file:
```bash
return [
    'root' => env('STAGING_ROOT'),
    'production' => env('PRODUCTION_ROOT'),
    'mail_always_to_address' => env('MAIL_ALWAYS_TO_ADDRESS')
];
```

You should add a disk named snapshots to app/config/filesystems.php on which the snapshots will be saved. This would be a typical configuration:
```bash
'snapshots' => [
  'driver' => 'local',
  'root' => database_path('snapshots'),
],
```

You should add a connexion named mysql-staging to app/config/databases.php. This would be a typical configuration:
```bash
'mysql-staging' => [
  'driver' => 'mysql',
  'url' => env('DATABASE_URL'),
  'host' => env('DB_HOST', '127.0.0.1'),
  'port' => env('DB_PORT', '3306'),
  'database' => 'forge',
  'username' => 'forge',
  'password' => '',
  'unix_socket' => env('DB_SOCKET', ''),
  'charset' => 'utf8mb4',
  'collation' => 'utf8mb4_unicode_ci',
  'prefix' => '',
  'prefix_indexes' => true,
  'strict' => true,
  'engine' => 'innoDB',
  'options' => extension_loaded('pdo_mysql') ? array_filter([
    PDO::MYSQL_ATTR_SSL_CA => env('MYSQL_ATTR_SSL_CA'),
  ]) : [],
],
```

You should add this cron to app/Console/Kernel.php
```bash
$schedule->command('slym:staging:reset')->daily();
```

## Credits

- [Yannik](https://slym.io/)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
