<?php

return [
    'root' => env('STAGING_ROOT'),
    'production' => env('PRODUCTION_ROOT'),
    'mail_always_to_address' => env('MAIL_ALWAYS_TO_ADDRESS'),
    'middleware' => explode(',', env('STAGING_MIDDLEWARE')),

    'backup' => [
        'sftp' => [
            'driver' => 'sftp',
            'host' => env('SFTP_HOST'),
            'username' => env('SFTP_USERNAME'),
            'password' => env('SFTP_PASSWORD'),
            'visibility' => 'private', // `private` = 0600, `public` = 0644
            'directory_visibility' => 'private', // `private` = 0700, `public` = 0755
            'port' => env('SFTP_PORT', 22),
            'root' => env('SFTP_ROOT', ''),
        ],
    ],
];
