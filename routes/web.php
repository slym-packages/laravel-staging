<?php

use Illuminate\Support\Facades\Route;
use Slym\LaravelStaging\Http\Controllers\ResetController;

Route::middleware(config('staging.middleware'))->group(function () {
    Route::get('staging/reset', ResetController::class);
});
